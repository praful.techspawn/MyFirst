﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class MovingObstacle : MonoBehaviour {

	public float moveSpeed = 3, rotationSpeed = 25;
	public List<Transform> wayPoint = new List<Transform>();
	public int point, targetPoint;
	public Vector3 moveDir;
	public float distance;
	private float newRotationSpeed;
	private Transform thisTransform;

	void Start () {
		thisTransform = transform;
		point = targetPoint-1;
	}

	void FixedUpdate () 
	{
		if(point < wayPoint.Count && wayPoint.Count >= 1)
		{
			moveDir = wayPoint[point].position - transform.position;

			distance = moveDir.magnitude;
			
			if(distance < 0.1F)
				point ++;
			else
				thisTransform.Translate(moveDir.normalized * moveSpeed * Time.deltaTime, Space.World);
		}
		else
			point = 0;


		if(moveDir.x < 0)
			newRotationSpeed = rotationSpeed;
		else
			newRotationSpeed = -rotationSpeed;

		transform.Rotate(Vector3.forward * newRotationSpeed * 100 * Time.deltaTime);
	}
}
